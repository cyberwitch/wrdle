import { FunctionComponent } from "react";

interface MessageProps {
  message: string;
};

export const Message: FunctionComponent<MessageProps> = props => {
  return (
    <div>
      {props.message}
    </div>
  )
};
