import { FunctionComponent } from "react";

export enum Correctness {
  Correct = "Correct",
  Misplaced = "Misplaced",
  Incorrect = "Incorrect"
};

interface LetterProps {
  letter: string;
  correctness?: Correctness;
};

export const Letter: FunctionComponent<LetterProps> = props => {
  const classes = "Letter " + (props.correctness && Correctness[props.correctness]);
  return (
    <span className={classes}>{props.letter}</span>
  );
};
