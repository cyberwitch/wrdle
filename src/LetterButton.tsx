import { FunctionComponent } from "react";

export enum Correctness {
  Correct = "Correct",
  Misplaced = "Misplaced",
  Incorrect = "Incorrect"
};

interface LetterButtonProps {
  letter: string;
  type: (letter: string) => void;
  correctness?: Correctness;
  wide?: boolean;
};

export const LetterButton: FunctionComponent<LetterButtonProps> = props => {
  const classes = `LetterButton ${props.wide && 'Wide '} ${props.correctness && Correctness[props.correctness]}`;
  return (
    <button className={classes} onClick={() => props.type(props.letter)}>{props.letter}</button>
  );
};
