import { Correctness } from "./Letter";
import { Letter } from "./Letter";
import { WORD_LENGTH } from './constants';
import { useAppSelector } from "./hooks";

interface Occurrences {
  [key: string]: number;
}

export const Rows = () => {
  const rowIndex = useAppSelector(state => state.guesses.rowIndex);
  const rows = useAppSelector(state => state.guesses.rows);
  const word = useAppSelector(state => state.guesses.word);
  
  const renderRow = (row: string, entered: boolean) => {
    let occurrences: Occurrences = {};
    word.split('').forEach(letter => {
      occurrences[letter] = occurrences[letter] ? occurrences[letter] + 1 : 1;
    });

    let completedRow = row.padEnd(WORD_LENGTH, "\u00a0").split('').map((letter, i) => {
      let correctness;

      if (entered) {
        if (letter === word.charAt(i)) {
          correctness = Correctness.Correct;
          occurrences[letter]--;
        } else if (!word.includes(letter)) {
          correctness = Correctness.Incorrect;
        }
      }

      return <Letter letter={letter} correctness={correctness} key={i} />
    });

    if (entered) {
      completedRow = completedRow.map((letter, i) => {
        if (!letter.props.correctness) {
          if (occurrences[letter.props.letter] > 0) {
            occurrences[letter.props.letter]--;
            return <Letter {...letter.props} correctness={Correctness.Misplaced} key={i} />
          } else {
            return <Letter {...letter.props} correctness={Correctness.Incorrect} key={i} />
          }
        }

        return letter;
      });
    }

    return completedRow;
  };
  
  return (
    <>
      {rows.map((row, i) => <div className="Row" key={i}>{renderRow(row, rowIndex > i)}</div>)}
    </>
  );
};
