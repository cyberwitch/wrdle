import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import { ROWS, WORD_LENGTH } from './constants';
import { validAnswers, validGuesses } from './words';

export enum Result {
  START,
  GUESSED_WRONG,
  TOO_SHORT,
  INVALID_WORD,
  WON,
  LOST,
};

export interface GuessesState {
  word: string;

  result: Result;
  rowIndex: number;
  rows: string[];

  correct: string;
  misplaced: string;
  incorrect: string;
};

const initialState: GuessesState = {
  word: validAnswers[Math.floor(Math.random() * validAnswers.length)],

  result: Result.START,
  rowIndex: 0,
  rows: Array.from({length: ROWS}, () => ""),

  correct: '',
  misplaced: '',
  incorrect: '',
};

const isDone = (state: GuessesState) => {
  return state.result === Result.WON || state.result === Result.LOST;
};

const getCurrentGuess = (state: GuessesState) => {
  return state.rows[state.rowIndex];
};

export const guessesSlice = createSlice({
  name: 'guesses',
  initialState,
  reducers: {
    enter: state => {
      if (!isDone(state)) {
        const guess = getCurrentGuess(state);

        if (guess.length !== WORD_LENGTH) {
          state.result = Result.TOO_SHORT;
        } else if (!validGuesses.includes(guess)) {
          state.result = Result.INVALID_WORD;
        } else {
          guess.split('').forEach((letter, i) => {
            if (state.word[i] === letter) {
              state.correct += letter;
            } else if (state.word.includes(letter)) {
              state.misplaced += letter;
            } else {
              state.incorrect += letter;
            }
          });

          state.rowIndex++;

          if (state.word === guess) {
            state.result = Result.WON;
          } else if (state.rowIndex === ROWS) {
            state.result = Result.LOST;
          } else {
            state.result = Result.GUESSED_WRONG;
          }
        }
      }
    },
    popLetter: state => {
      const guess = getCurrentGuess(state);
      if (!isDone(state) && guess.length > 0) {
        state.rows[state.rowIndex] = guess.slice(0, guess.length - 1);
      }
    },
    pushLetter: (state, action: PayloadAction<string>) => {
      if (!isDone(state) && getCurrentGuess(state).length < WORD_LENGTH) {
        state.rows[state.rowIndex] += action.payload;
      }
    },
  },
});

export const { enter, popLetter, pushLetter } = guessesSlice.actions;
