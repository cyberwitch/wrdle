import { useEffect, useState } from 'react';

import './App.css';
import { Keyboard } from './Keyboard';
import { Message } from './Message';
import { Result } from './guessesSlice';
import { Rows } from './Rows';
import { useAppSelector } from './hooks';

export const App = () => {
  const [message, setMessage] = useState('Go ahead and guess!');

  const word = useAppSelector(state => state.guesses.word);
  const result = useAppSelector(state => state.guesses.result);

  useEffect(() => {
    console.log("Today's word is " + word);
  }, [word]);

  useEffect(() => {
    if (result === Result.GUESSED_WRONG) {
      setMessage('Keep trying!');
    } else if (result === Result.INVALID_WORD) {
      setMessage('That\'s not a real word.');
    } else if (result === Result.LOST) {
      setMessage('Better luck next time. The word was ' + word);
    } else if (result === Result.TOO_SHORT) {
      setMessage('That word is too short.');
    } else if (result === Result.WON) {
      setMessage('Good job!');
    }
  }, [result, word]);

  return (
    <div className="App">
      <header className="App-header">
        <h3>Wrdle - a Wordle clone by cyberwitch</h3>
        <Message message={message}/>
        <Rows />
        <Keyboard />
      </header>
    </div>
  );
}
