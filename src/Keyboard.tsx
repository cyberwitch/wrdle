import { enter, popLetter, pushLetter } from './guessesSlice';
import { useAppDispatch, useAppSelector } from './hooks';
import { Correctness, LetterButton } from "./LetterButton";

export const Keyboard = () => {
  const dispatch = useAppDispatch();

  const correct = useAppSelector(state => state.guesses.correct);
  const misplaced = useAppSelector(state => state.guesses.misplaced);
  const incorrect = useAppSelector(state => state.guesses.incorrect);
  
  const getRow = (letters: string) => {
    return letters.split("").map((letter, i) => {
      let correctness;

      if (correct.includes(letter)) {
        correctness = Correctness.Correct;
      } else if (misplaced.includes(letter)) {
        correctness = Correctness.Misplaced;
      } else if (incorrect.includes(letter)) {
        correctness = Correctness.Incorrect;
      }
      
      return <LetterButton letter={letter} type={() => dispatch(pushLetter(letter))} correctness={correctness} key={i} />
    });
  };

  return (
    <div className="Keyboard">
      <div className="KeyboardRow">
        {getRow("qwertyuiop")}
      </div>
      <div className="KeyboardRow">
        <div className="KeyboardSpacer" />
        {getRow("asdfghjkl")}
        <div className="KeyboardSpacer" />
      </div>
      <div className="KeyboardRow">
        <LetterButton letter="&crarr;" type={() => dispatch(enter())} wide={true} />
        {getRow("zxcvbnm")}
        <LetterButton letter="&laquo;" type={() => dispatch(popLetter())} wide={true} />
      </div>
    </div>
  );
};
